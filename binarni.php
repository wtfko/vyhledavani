<?php
// zadani (hodnoty musi mit lichy pocet, musi byt serazeny vzestupne)
$hodnoty = array(1, 2, 3, 4, 5, 6, 7);
$hledame = 5;

// prochazej pole
$prochazej = true;
while ($prochazej) {

	// vypocitani poloviny pole
	$pocet_hodnot = count($hodnoty) - 1;
	$polovina = ($pocet_hodnot / 2);
	$polovina = $hodnoty[$polovina];

	// overeni zda nebyla hodnota nalezena
	if ((count($hodnoty) == 1 && $hodnoty[0] == $hledame) || $polovina == $hledame) {
		echo "nalezeno";
		$prochazej = false;
		
	// rozdeleni pole, pokud je hledana hodnta mensi nez stred
	} else if ($hledame < $polovina) {
		$docasne_hodnoty = array();
		foreach ($hodnoty as $hodnota) {
			if ($hodnota >= $polovina) {
				break;
			}
			$docasne_hodnoty[] = $hodnota;
		}
		$hodnoty = $docasne_hodnoty;

	// rozdeleni pole, pokud je hledana hodnta v�tsi nez stred	
	} else {
		$docasne_hodnoty = array();
		foreach ($hodnoty as $klic => $hodnota) {
			if ($hodnota > $polovina) {
				$docasne_hodnoty[] = $hodnota;
			}
		}
		$hodnoty = $docasne_hodnoty;
	}
}
